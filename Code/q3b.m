font_size = 18;
% to ensure diagonal version
load('diag_covar.mat');
size(approx_covar)

skill = [-5.0:0.001:5.0]';
y_fan_hui   = mvnpdf(skill, approx_mean(fan_hui_id), approx_covar(fan_hui_id));
y_lee_sedol = mvnpdf(skill, approx_mean(lee_sedol_id), approx_covar(lee_sedol_id));
y_alpha_go  = mvnpdf(skill, approx_mean(alpha_go_id), approx_covar(alpha_go_id));

%plotting diagonal
subplot(1,2,1);
title('Diagonal Covariance', 'FontSize', font_size + 4);

hold on;
plot(skill, y_fan_hui, 'g', 'DisplayName', 'Fan Hui', 'LineWidth',2);
plot(skill, y_lee_sedol, 'b', 'DisplayName', 'Lee Sedol', 'LineWidth',2);
plot(skill, y_alpha_go, 'r', 'DisplayName', 'Alhpa Go', 'LineWidth',2);

%formatting
xlabel('player skill', 'FontSize', font_size);
ylabel('Approximate diagonal posterior PDF', 'FontSize', font_size);
h_legend = legend('-DynamicLegend', 'Location','northeast');
set(h_legend,'FontSize', font_size + 2);
ylim([0 8]);
hold off;

% to ensure full version
load('full_covar.mat');
size(approx_covar)

skill = [-5.0:0.001:5.0]';
y_fan_hui   = mvnpdf(skill, approx_mean(fan_hui_id), approx_covar(fan_hui_id, fan_hui_id));
y_lee_sedol = mvnpdf(skill, approx_mean(lee_sedol_id), approx_covar(lee_sedol_id, lee_sedol_id));
y_alpha_go  = mvnpdf(skill, approx_mean(alpha_go_id), approx_covar(alpha_go_id, alpha_go_id));

%plotting full
subplot(1,2,2);
title('Full Covariance', 'FontSize', font_size + 4);

hold on;
plot(skill, y_fan_hui, 'g', 'DisplayName', 'Fan Hui', 'LineWidth',2);
plot(skill, y_lee_sedol, 'b', 'DisplayName', 'Lee Sedol', 'LineWidth',2);
plot(skill, y_alpha_go, 'r', 'DisplayName', 'Alhpa Go', 'LineWidth',2);
%formatting
xlabel('player skill', 'FontSize', font_size);
ylabel('Approximate full posterior PDF', 'FontSize', font_size);
h_legend = legend('-DynamicLegend', 'Location','northeast');
set(h_legend,'FontSize',font_size + 2);
ylim([0 8]);
hold off;
