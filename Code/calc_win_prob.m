%it assumes that either diag_covar.mat or full_covar.mat was loaded
function prob = calc_win_prob(m, V, n_players,...
  performance_var, alpha_go_id, lee_sedol_id) 
  x = zeros(n_players, 1);
  %performance_var under squr because we are interested in beta
  %which is standard deviation
  %playing black that is why +1 sign
  x(alpha_go_id)  =  1/sqrt(2 * performance_var); 
  %playing white that is why -1 sign
  x(lee_sedol_id) = -1/sqrt(2 * performance_var);
  %equation 
  size(x)
  size(m)
  size(V)
  arg = m'*x / sqrt(x'*V*x + 1);
  prob = normcdf(arg);
end