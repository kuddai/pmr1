beta = 1;
bonus = 2.0;
skill_diff = -5.0:0.01:5.0;
prob_black_wins = normcdf((skill_diff + bonus)/sqrt(2 * beta ^ 2));

figure();
plot(skill_diff, prob_black_wins);
title('Normal CDF factor accounting black starts first');
xlabel('difference in players skill');
ylabel('Probability that black wins');